export const environment = {
  production: true,
  apiUrl: window["env"]["apiUrl"] || "http://ayd2-practica2-250903952.us-east-2.elb.amazonaws.com",
  debug: window["env"]["debug"] || false,
  user: window["env"]["user-auth_HOSTNAME"] || "http://localhost",
  catalogo: window["env"]["catalogmicroservice_HOSTNAME"]  || "http://localhost",
  ordenes:  window["env"]["ordenesmicroservicio_HOSTNAME"] || "http://localhost",
};
