import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private logUsr = {
    username : '',
    password : ''
  }

  constructor(private apiService : ApiService, private router: Router) { }

  ngOnInit(): void {
  }


  user(event: any) {
    this.logUsr.username = event.target.value;
  }

  password(event: any) {
    this.logUsr.password = event.target.value;
  }

  signin(){
    console.log(this.logUsr);

    this.apiService.signin(this.logUsr).subscribe(
      (res)=>{
        console.log(res);
        if(res.success){
          var bearer = res.data.accessToken;
          if(bearer!=null)
          {
            this.apiService.setuser(bearer);
          }
          else
          {
            alert('Datos icorrectos!');
          }
        }
        else{
          alert('Inténtelo de nuevo!');
        }
      },
      (err)=>{
        alert('Inténtelo de nuevo!');
        console.log(err);
      }
    );
  }

}
