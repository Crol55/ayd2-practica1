import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';

import { UserGuard } from './user.guard';
import { AdminGuard } from './admin.guard';


const routes: Routes = [
  {path : 'login', component : LoginComponent},
  {path : 'signup', component : SignupComponent,},
  {path : 'home', component : HomeComponent, canActivate : [UserGuard]},
  {path : 'admin', component : AdminComponent, canActivate : [AdminGuard]},
  {path : '**', component: HomeComponent, canActivate : [UserGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
