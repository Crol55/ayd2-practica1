import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {

  carrito:any;
  cantidadTotal = 0;
  direccion = "";
  currentUser:any;
  isUser : boolean;

  constructor(private apiService : ApiService, public modalRef: MDBModalRef, private router: Router) { 
    this.currentUser = this.apiService.currentUser();
    this.isUser = this.currentUser == false ? false : true;
   }

  ngOnInit(): void {
    this.cargarCarrito()
  }

  cargarCarrito()
  {
    this.carrito = this.apiService.getCarrito();
    console.log(this.carrito);
    this.cantidadTotal = 0;
    for (let c of this.carrito) {
      this.cantidadTotal += c.cantidad * (c.Price - (c.Price * c.Discount/100));
    }
  }

  restarCarrito(data : any){
    this.apiService.restarCarrito(data);
    this.cargarCarrito();
  }

  direction(event: any) {
    this.direccion = event.target.value;
  }

  realizarOrden(){
    this.modalRef.hide();
    console.log(this.direccion)
    let data = {
      id : 0,
      tipo : "Restaurante",
      direccion : this.direccion,
      precio_total : this.cantidadTotal,
      productos : []
    };
    data.id = this.currentUser == false ? 0 : this.currentUser.id;

    data.tipo = data.direccion != "" ? "A Domicilio" : "Restaurante";

    for(let producto of this.carrito){
      data.productos.push({id : producto.idCatalog, cantidad : producto.cantidad});
    }
    console.log(data)
    this.apiService.addOrder(data).subscribe(
      (data)=>{
        console.log(data);
        alert('Orden Realizada.');
        this.apiService.limpiarCarrito();
        this.router.navigate(['/']);
      },
      (err)=>{
        console.error(err);
      }
    );

  }

}
