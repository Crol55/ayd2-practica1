import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {ProductoComponent} from '../producto/producto.component';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  modalRef: MDBModalRef;

  public currentUser;
  public isUser : boolean = true;
  public productosT = [];
  public productos = [];
  public ordenes = [];
  
  constructor(private apiService : ApiService,  private modalService: MDBModalService) { 
    this.currentUser = this.apiService.currentUser();
    console.log(this.currentUser);
    if(this.currentUser == false)
    {
      this.isUser = false;
    }
    else
    {
      console.log(this.currentUser)
      console.log(this.currentUser.id)
      this.getOrders(this.currentUser.id);
    }
  }

  ngOnInit(): void {
    this.getProductos();
  }

  getProductos(){
    this.apiService.getProducts().subscribe(
      (data)=>{
        console.log(data);
        this.productosT = this.clasificarProductos(data);
        console.log('Productos Clasificados');
        console.log(this.productosT);
        this.productos = this.clasificarProductos(data);
      },
      (err)=>{
        console.error(err);
      }
    )
  }

  clasificarProductos(listaProductos){
    let productos = [];
    for(let product of listaProductos)
    {
      let productoClasificado = false;
      for(let categoria of productos)
      {
        if(categoria.categoria == product.Category)
        {
          categoria.productos.push(product);
          productoClasificado = true;
          break;
        }
      }
      if(!productoClasificado)
      {
        productos.push({ categoria:product.Category, productos: [product] })
      }
    }
    return productos;
  }

  productName(event: any) {
    let nombre = event.target.value;
    var tam = nombre.length;
    let productos = [];
    if(nombre == '')
    {
      this.productos = this.productosT;
      return
    }
    for(let categoria of this.productosT)
    {
      productos.push({ categoria:categoria.categoria, productos: [] })
      for(let producto of categoria.productos)
      {
        var i = 0;
        var ver = true;
        while(i < tam)
        {
          if(i == producto.ProductName.length)
          {
            ver = false;
            break;
          }
          if(producto.ProductName[i].toLowerCase()!=nombre[i].toLowerCase())
          {
            ver = false;
            break;
          }
          i++;
        }
        if(ver)
        {
          console.log(productos)
          productos[productos.length-1].productos.push(producto);
        }
      }
    }
    this.productos = productos;
  }

  agregarCarrito(data : any){
    alert(`${data.ProductName} agregado al carrito!`);
    this.apiService.agregarCarrito(data);
    this.getOrders(this.currentUser.id);
  }

  getOrders(user){
    this.apiService.getOrder(user).subscribe(
      (data)=>{
        console.log(data);
        this.ordenes = data.orden;
        this.ajustarFecha();
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  ajustarFecha(){
    for(let fecha of this.ordenes){
      fecha.fecha = fecha.fecha.split('GMT')[0];
      if(fecha.estado == "Nueva orden"){
        fecha.seCancela = true;
      }else{
        fecha.seCancela = false;
      }
    }
  }

  cancelar(data){
    let datos = {
      orden_id : data.orden_id,
      estado : "Cancelada"
    }
    this.apiService.updateOrder(datos).subscribe(
      (data)=>{
        console.log(data);
        alert(`Orden Cancelada.`);
        this.getOrders(this.currentUser.id);
      },
      (err)=>{
        console.error(err);
      }
    );
  }
  actualizar(){
    this.getOrders(this.currentUser.id);
  }
  actualizarP(){
    this.getProductos();
  }

  openProducto(producto) {
    this.apiService.setProducto(producto);
    this.modalRef = this.modalService.show(ProductoComponent)
  }
}
