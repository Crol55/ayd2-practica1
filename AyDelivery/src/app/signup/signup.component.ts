import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { throwError, from } from 'rxjs';
import { formatCurrency } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public newUsr = {
    fullname : '',
    username : '',
    email : '',
    password : '',
    confirmPass : ''
  }

  form = new FormGroup({
    role: new FormControl('ROLE_CLIENT'),
  });

  constructor(private apiService : ApiService, private router: Router) { }

  ngOnInit(): void {}

  fullname(event: any){
    this.newUsr.fullname = event.target.value;
  }

  user(event: any) {
    this.newUsr.username = event.target.value;
  }

  email(event: any){
    this.newUsr.email = event.target.value;
  }

  //Handler Password
  pass(event: any) {
    this.newUsr.password = event.target.value;
  }

  //Handler ConfirmPassword
  confirmPass(event: any){
    this.newUsr.confirmPass = event.target.value;
  }

  signup(){
    console.log(this.newUsr);
    if(this.verificarPass(this.newUsr.password,this.newUsr.confirmPass)){
      let usuario = {
        name : this.newUsr.fullname,
        username : this.newUsr.username,
        email : this.newUsr.email,
        password : this.newUsr.password,
        role : this.form.get('role').value
      }
      console.log(usuario);
      this.apiService.signup(usuario).subscribe(
        (res)=>{
          console.log(res);
          if(res.success){
            alert("Usuario creado");
            this.router.navigate(['/login']);
          }
          else{
            alert('Ya existe el usuario');
          }
        },
        (err)=>{
          console.log(err);
        }
      );
    }
    else{
      alert("Las contrasenas no coindicen");
    }
  }

  verificarPass(password: string, confirmPass : string){
    if(password != confirmPass){
      alert('Las contrasenias no coinciden');
      return false;
    }
    return true;
  }

}
