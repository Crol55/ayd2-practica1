import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs/internal/Observable'
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  headers:HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json"
  });

  user : string = `${environment.user}:8080/ayd2/practica1/v1`;
  catalogo : string = `${environment.catalogo}:3000`;
  ordenes : string = `${environment.ordenes}:3100`;

  constructor(private http : HttpClient, private router: Router) { }

  /* USER AUTHENTICATION */

  public signin(data:any): Observable<any>{
    return this.http.post(`${this.user}/auth/signin`, data);
  }

  public signup(data : any) : Observable<any> {
    return this.http.post(`${this.user}/auth/signup`,data);
  }

  public getuser(data : any) : Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${data}`
    })
    return this.http.get(`${this.user}/users`, { headers : headers});
  }

  /* CATALOGO */

  public addProduct(data : any) : Observable<any> {
    return this.http.post(`${this.catalogo}/insertProduct`,data);
  }

  public updateProductName(data : any) : Observable<any> {
    return this.http.put(`${this.catalogo}/updateProductName`,data);
  }

  public updateProductImagePath(data : any) : Observable<any> {
    return this.http.put(`${this.catalogo}/updateProductImagePath`,data);
  }

  public updateProductDescription(data : any) : Observable<any> {
    return this.http.put(`${this.catalogo}/updateProductDescription`,data);
  }

  public updateProductPrice(data : any) : Observable<any> {
    return this.http.put(`${this.catalogo}/updateProductPrice`,data);
  }

  public updateProductDiscount(data : any) : Observable<any> {
    return this.http.put(`${this.catalogo}/updateProductDiscount`,data);
  }

  public updateProductCategory(data : any) : Observable<any> {
    return this.http.put(`${this.catalogo}/updateProductCategory`,data);
  }

  public deleteProduct(data : any) : Observable<any> {
    return this.http.put(`${this.catalogo}/deleteProduct`,data);
  }

  public getProducts() : Observable<any> {
    return this.http.get(`${this.catalogo}/get_allProducts`);
  }

  /* ORDENES */

  public addOrder(data : any) : Observable<any> {
    return this.http.post(`${this.ordenes}/ordenes`,data);
  }

  public allOrders() : Observable<any> {
    return this.http.get(`${this.ordenes}/ordenes/getall`);

  }

  public getOrder(id : any) : Observable<any> {
    return this.http.get(`${this.ordenes}/ordenes/usuario?users_id=${id}`);
  }

  public updateOrder(data : any) : Observable<any> {
    return this.http.put(`${this.ordenes}/ordenes`,data);
  }

  public deleteOrder(id : any) : Observable<any> {
    return this.http.delete(`${this.ordenes}/ordenes/deleteOrden?id=${id}`);
  }


  /* OBTENER USUARIO ACTUAL */

  public currentUser(){
    let user_string = localStorage.getItem('user');
    if(!isNullOrUndefined(user_string)){
      let user = JSON.parse(user_string);
      return user;
    }
    return false;
  }



  public setuser(bearer:any){
    this.getuser(bearer).subscribe(
      (res)=>{
        console.log(res);
        if(res.success)
        {
          let user_string = JSON.stringify(res.data);
          localStorage.setItem('user',user_string);
          let user = this.currentUser();
          console.log(user);
          alert(`Bienvenido ${user.name}!`);
          if( user.roles[0].id == 2 )
          {
            this.limpiarCarrito();
            this.router.navigate(['/home']);
          }
          else if ( user.roles[0].id == 1 )
          {
            this.router.navigate(['/admin']);
          }
          else
          {
            alert('Inténtelo de nuevo!');
          }
        }
      },
      (err)=>{
        console.log(err);
      }
    );
  }
  
  public removeUser(){
    localStorage.removeItem('user');
  }

  /* CARRITO */

  public limpiarCarrito(){
    localStorage.removeItem('carrito');
    let carrito = JSON.stringify([]);
    localStorage.setItem('carrito',carrito);
  }

  public agregarCarrito(producto:any){
    let carrito_string = localStorage.getItem('carrito');
    let carrito = JSON.parse(carrito_string);
    if(carrito == null)
    {
      carrito = [];
    }
    else
    {
      for (let c of carrito) {
        if( producto.idCatalog == c.idCatalog )
        {
          console.log(producto.cantidad);
          if(producto.cantidad != null || producto.cantidad != undefined)
          {
            c.cantidad = Number(c.cantidad) +  Number(producto.cantidad);
          }
          else
          {
            c.cantidad++;
          }
          let carrito_string2 = JSON.stringify(carrito);
          localStorage.removeItem('carrito');
          localStorage.setItem('carrito',carrito_string2);
          console.log(carrito);
          return;
        }
      }
    }
    if(producto.cantidad == null || producto.cantidad == undefined)
    {
      producto.cantidad = 1;
    }
    carrito.push(producto);
    let carrito_string2 = JSON.stringify(carrito);
    localStorage.removeItem('carrito');
    localStorage.setItem('carrito',carrito_string2);
    console.log(carrito);
  }

  public setProducto(producto:any){
    let producto_string = JSON.stringify(producto);
    localStorage.setItem('producto', producto_string);
    console.log(producto);
  }

  public restarCarrito(producto:any){
    let carrito_string = localStorage.getItem('carrito');
    let carrito = JSON.parse(carrito_string);
    if(carrito == null)
    {
      carrito = [];
    }
    else
    {
      var contador = 0;
      for (let c of carrito) {
        if( producto.idCatalog == c.idCatalog )
        {
          c.cantidad--;
          if(c.cantidad == 0)
          {
            carrito.splice( contador, 1 );
          }
          let carrito_string2 = JSON.stringify(carrito);
          localStorage.removeItem('carrito');
          localStorage.setItem('carrito',carrito_string2);
          console.log(carrito);
          return;
        }
        contador ++;
      }
    }
  }

  public getCarrito(){
    let carrito_string = localStorage.getItem('carrito');
    let carrito = JSON.parse(carrito_string);
    return carrito;
  }

  public getProducto(){
    let producto_string = localStorage.getItem('producto');
    let producto = JSON.parse(producto_string);
    return producto;
  }
}
 