import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
 
  public currentUser;
  public productos = [];
  public title;
  public fases = true;
  public eliminar = false;

  public Orders;
  public newOrders;
  public preparedOrders;
  public wayOrders;
  public deliveredOrders;
  public cancelledOrders;
  public paidOrders;

  private strProducto = {
    ProductName : '',
    ImagePath : '',
    Description : '',
    Price : '',
    Discount : '',
    Category: ''
  }

  constructor(private apiService : ApiService, private router: Router) { 
    this.currentUser = this.apiService.currentUser();
    console.log(this.currentUser);
  }


  ngOnInit(): void {
    this.getProductos();
    this.getOrders();
  }

  productName(event: any) {
    this.strProducto.ProductName = event.target.value;
  }

  imagePath(event: any) {
    this.strProducto.ImagePath = event.target.value;
  }

  description(event: any) {
    this.strProducto.Description = event.target.value;
  }

  price(event: any) {
    this.strProducto.Price = event.target.value;
  }

  discount(event: any) {
    this.strProducto.Discount = event.target.value;
  }

  category(event: any) {
    this.strProducto.Category = event.target.value;
  }

  agregarProducto(){
    if(this.strProducto.ProductName == '' || this.strProducto.ImagePath == '' || this.strProducto.Description == '' || this.strProducto.Price == '')
    {
      alert('INGRESE TODOS LOS CAMPOS.');
      return
    }
    this.apiService.addProduct(this.strProducto).subscribe(
      (data)=>{
        console.log(data);
        if(data.status == 'Producto Agregado'){
          alert("Producto Agregado correctamente");
          this.getProductos();
        }
        else{
          alert(data.message);
        }
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  getProductos(){
    this.apiService.getProducts().subscribe(
      (data)=>{
        console.log(data);
        this.productos = data;
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  modificarNombre(data:any){
    var cambio = prompt("Modificar Nombre:", data.ProductName);
    if (cambio != null && cambio != "") {
      let datos = {
        ProductName : cambio,
        idCatalog : data.idCatalog
      };
      this.apiService.updateProductName(datos).subscribe(
        (data)=>{
          console.log(data);
          alert(data.status);
          this.getProductos();
        },
        (err)=>{
          console.error(err);
        }
      );
    }
  }

  modificarImagen(data:any){
    var cambio = prompt("Modificar Direccion de Imagen:", data.ImagePath);
    if (cambio != null && cambio != "") {
      let datos = {
        ImagePath : cambio,
        idCatalog : data.idCatalog
      };
      this.apiService.updateProductImagePath(datos).subscribe(
        (data)=>{
          console.log(data);
          alert(data.status);
          this.getProductos();
        },
        (err)=>{
          console.error(err);
        }
      );
    }
  }

  modificarDescripcion(data:any){
    var cambio = prompt("Modificar Descripcion:", data.Description);
    if (cambio != null && cambio != "") {
      let datos = {
        Description : cambio,
        idCatalog : data.idCatalog
      };
      this.apiService.updateProductDescription(datos).subscribe(
        (data)=>{
          console.log(data);
          alert(data.status);
          this.getProductos();
        },
        (err)=>{
          console.error(err);
        }
      );
    }
  }

  modificarPrecio(data:any){
    var cambio = prompt("Modificar Precio:", data.Price);
    if (cambio != null && cambio != "") {
      let datos = {
        Price : cambio,
        idCatalog : data.idCatalog
      };
      this.apiService.updateProductPrice(datos).subscribe(
        (data)=>{
          console.log(data);
          alert(data.status);
          this.getProductos();
        },
        (err)=>{
          console.error(err);
        }
      );
    }
  }

  modificarDescuento(data:any){
    var cambio = prompt("Modificar Descuento %:", data.Discount);
    if (cambio != null && cambio != "") {
      let datos = {
        Discount : cambio,
        idCatalog : data.idCatalog
      };
      this.apiService.updateProductDiscount(datos).subscribe(
        (data)=>{
          console.log(data);
          alert(data.status);
          this.getProductos();
        },
        (err)=>{
          console.error(err);
        }
      );
    }
  }

  modificarCategoria(data:any){
    var cambio = prompt("Modificar Categoria:", data.Category);
    if (cambio != null && cambio != "") {
      let datos = {
        Category : cambio,
        idCatalog : data.idCatalog
      };
      this.apiService.updateProductCategory(datos).subscribe(
        (data)=>{
          console.log(data);
          alert(data.status);
          this.getProductos();
        },
        (err)=>{
          console.error(err);
        }
      );
    }
  }

  eliminarProducto(data:any){
    let datos = {
      idCatalog : data.idCatalog
    };
    this.apiService.deleteProduct(datos).subscribe(
      (data)=>{
        console.log(data);
        alert(data.status);
        this.getProductos();
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  getOrders(){
    this.apiService.allOrders().subscribe(
      (data)=>{
        console.log("ORDENES")
        console.log(data.orden);
        this.ajustarFecha(data.orden);
        this.clasificar(data.orden);
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  clasificar(data){
    let nueva:any[]=[];
    let prepa:any[]=[];
    let camino:any[]=[];
    let entregado:any[]=[];
    let cancelado:any[]=[];
    let pagado:any[] = [];
    for(let orden of data){
      switch(orden.estado){
        case "Nueva orden":{
          nueva.push(orden);
          break;
        }
        case "En Preparacion":{
          prepa.push(orden);
          break;
        }
        case "En Camino":{
          camino.push(orden);
          break;
        }
        case "Entregado":{
          entregado.push(orden);
          break;
        }
        case "Cancelada":{
          cancelado.push(orden);
          break;
        }
        case "Pagada":{
          pagado.push(orden);
          break;
        }
      }
    }
    this.newOrders = nueva;
    this.preparedOrders = prepa;
    this.wayOrders = camino;
    this.deliveredOrders = entregado;
    this.cancelledOrders = cancelado;
    this.paidOrders = pagado;
    
    switch(this.title){
      case "Nueva orden":{
        this.getNueva();
        break;
      }
      case "En Preparacion":{
        this.getPreparacion();
        break;
      }
      case "En Camino":{
        this.getCamino();
        break;
      }
      case "Entregado":{
        this.getEntregados();
        break;
      }
      case "Cancelada":{
        this.getCanceladas();
        break;
      }
      case "Pagada":{
        this.getPagadas();
        break;
      }
    }
  }

  getNueva(){
    this.title = "Nueva orden";
    this.Orders = this.newOrders;
    this.fases = true;
    this.eliminar = false;
  }

  getPreparacion(){
    this.title = "En Preparacion";
    this.Orders = this.preparedOrders;
    this.fases = true;
    this.eliminar = false;
  }

  getCamino(){
    this.title = "En Camino";
    this.Orders = this.wayOrders;
    this.fases = true;
    this.eliminar = false;
  }

  getEntregados(){
    this.title = "Entregado";
    this.Orders = this.deliveredOrders;
    this.fases = true;
    this.eliminar = false;
  }

  getCanceladas(){
    this.title = "Cancelada";
    this.Orders = this.cancelledOrders;
    this.fases = false;
    this.eliminar = true;
  }

  getPagadas(){
    this.title = "Pagada";
    this.Orders = this.paidOrders;
    this.fases = false;
    this.eliminar = false;
  }

  sigFase(data){
    let estado = '';
    switch(data.estado){
      case "Nueva orden":{
        estado = "En Preparacion";
        break;
      }
      case "En Preparacion":{
        estado = "En Camino";
        break;
      }
      case "En Camino":{
        estado = "Entregado";
        break;
      }
      case "Entregado":{
        estado = "Pagada"
        break;
      }
    }
    let datos = {
      orden_id : data.orden_id,
      estado : estado
    }

    this.apiService.updateOrder(datos).subscribe(
      (data)=>{
        console.log(data);
        alert(`Orden Movida a ${estado}.`);
        this.getOrders();
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  deleteOrder(data){
    this.apiService.deleteOrder(data.orden_id).subscribe(
      (data)=>{
        console.log(data);
        alert(`Orden eliminada.`);
        this.getOrders();
      },
      (err)=>{
        console.error(err);
      }
    );
  }

  ajustarFecha(data){
    for(let fecha of data){
      fecha.fecha = fecha.fecha.split('GMT')[0];
    }
  }

  actualizar(){
    this.getOrders();
  }

  logout(){
    this.apiService.removeUser();
    this.apiService.limpiarCarrito();
    this.router.navigate(['/login']);
  }
}
