import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {
  constructor(private router: Router, private apiService: ApiService){}
  
  
  canActivate(){
    let user : any = this.apiService.currentUser();
    if ( user != false )
    {
      let rol : any = user.roles[0].id;
    
      if(rol == '1'){
        this.router.navigate(['/admin']);
        return false;
      }
      
    }
    return true;
  }
  
}
