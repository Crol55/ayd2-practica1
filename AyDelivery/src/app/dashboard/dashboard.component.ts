import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

import {CarritoComponent} from '../carrito/carrito.component';
import { MDBModalRef, MDBModalService } from 'ng-uikit-pro-standard';

@Component({
  selector: 'app-cuenta',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  modalRef: MDBModalRef;

  public currentUser : any;
  public isUser : boolean = true;

  constructor(private apiService: ApiService, private router: Router, private modalService: MDBModalService) { 
    this.currentUser = this.apiService.currentUser();
    console.log(this.currentUser);
    if(this.currentUser == false)
    {
      this.isUser = false;
      this.currentUser = {
        name : 'Guest'
      }
    }
    
  }

  ngOnInit(): void {}

  openCarrito() {
    this.modalRef = this.modalService.show(CarritoComponent)
  }

  logout(){
    this.apiService.removeUser();
    this.apiService.limpiarCarrito();
    this.router.navigate(['/login']);
  }
}
