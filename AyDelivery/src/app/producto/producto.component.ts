import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  producto:any;
  cantidad:any;

  constructor(private apiService : ApiService, public modalRef: MDBModalRef, private router: Router) { 
    this.cargarProducto();
  }

  ngOnInit(): void {
    
  }

  cargarProducto()
  {
    this.producto = this.apiService.getProducto();
    console.log(this.producto);
  }

  agregarCarrito(){
    alert(`${this.producto.ProductName} agregado al carrito!`);
    this.producto.cantidad = this.cantidad;
    this.apiService.agregarCarrito(this.producto);
    this.modalRef.hide();
  }

  quantity(event: any) {
    this.cantidad = event.target.value;
  }

}
