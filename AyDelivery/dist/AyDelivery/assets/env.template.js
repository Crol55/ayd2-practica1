(function(window) {
    window.env = window.env || {};
  
    // Environment variables
    window["env"]["apiUrl"] = "${API_URL}";
    window["env"]["debug"] = "${DEBUG}";
    window["env"]["user-auth_HOSTNAME"] = "${user-auth_HOSTNAME}";
    window["env"]["catalogmicroservice_HOSTNAME"] = "${catalogmicroservice_HOSTNAME}";
    window["env"]["ordenesmicroservicio_HOSTNAME"] = "${ordenesmicroservicio_HOSTNAME}";
  })(this);