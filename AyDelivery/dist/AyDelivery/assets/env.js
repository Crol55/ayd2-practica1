(function(window) {
    window["env"] = window["env"] || {};
  
    // Environment variables
    window["env"]["apiUrl"] = "http://localhost";
    window["env"]["debug"] = true;
    window["env"]["user-auth_HOSTNAME"] = "http://localhost";
    window["env"]["catalogmicroservice_HOSTNAME"] = "http://localhost";
    window["env"]["ordenesmicroservicio_HOSTNAME"] = "http://localhost";
  })(this);