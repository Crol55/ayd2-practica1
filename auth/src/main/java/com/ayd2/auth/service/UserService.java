package com.ayd2.auth.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.ayd2.auth.entity.User;
import com.ayd2.auth.exception.AppException;
import com.ayd2.auth.payload.ApiResponse;
import com.ayd2.auth.repository.UserRepository;
import com.ayd2.auth.security.UserPrincipal;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public ApiResponse getUser() {
		return new ApiResponse(true, "success", getUserAuthenticated());
	}

	private User getUserAuthenticated() {
		UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		Optional<User> user = userRepository.findByUsername(userPrincipal.getUsername());

		if (!user.isPresent()) {
			throw new AppException("User authenticated not found");
		}

		return user.get();
	}

}
