package com.ayd2.auth.service;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ayd2.auth.entity.Role;
import com.ayd2.auth.entity.User;
import com.ayd2.auth.enums.RoleName;
import com.ayd2.auth.exception.AppException;
import com.ayd2.auth.payload.ApiResponse;
import com.ayd2.auth.payload.JwtAuthenticationResponse;
import com.ayd2.auth.payload.LoginRequest;
import com.ayd2.auth.payload.SignUpRequest;
import com.ayd2.auth.repository.RoleRepository;
import com.ayd2.auth.repository.UserRepository;
import com.ayd2.auth.security.JwtTokenProvider;

@Service
public class UserSecurityService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider tokenProvider;

	public ApiResponse signin(LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = tokenProvider.generateToken(authentication);
		return new ApiResponse(true, "success", new JwtAuthenticationResponse(jwt));
	}

	public ApiResponse signUp(SignUpRequest signUpRequest) {

		if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getUsername()))) {
			return new ApiResponse(false, "Username is already taken!");
		}

		if (Boolean.TRUE.equals(userRepository.existsByEmail(signUpRequest.getEmail()))) {
			return new ApiResponse(false, "Email is already taken!");
		}

		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				signUpRequest.getPassword());

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		Role userRole = roleRepository.findByName(RoleName.valueOf(signUpRequest.getRole()))
				.orElseThrow(() -> new AppException("User Role not set."));

		user.setRoles(Collections.singleton(userRole));

		userRepository.saveAndFlush(user);

		return new ApiResponse(true, "User registered successfully", user);
	}

}
