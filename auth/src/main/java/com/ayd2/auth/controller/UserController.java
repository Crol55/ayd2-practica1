package com.ayd2.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ayd2.auth.exception.AppException;
import com.ayd2.auth.payload.ApiResponse;
import com.ayd2.auth.service.UserService;
import com.ayd2.auth.util.AppConstants;

@RestController
@RequestMapping(AppConstants.BASE_URL + "/users")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping
	public ResponseEntity<ApiResponse> getUser() throws AppException {
		return ResponseEntity.ok(userService.getUser());
	}

}
