package com.ayd2.auth.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ayd2.auth.payload.ApiResponse;
import com.ayd2.auth.payload.LoginRequest;
import com.ayd2.auth.payload.SignUpRequest;
import com.ayd2.auth.service.UserSecurityService;
import com.ayd2.auth.util.AppConstants;

@RestController
@RequestMapping(AppConstants.BASE_URL + "/auth")
public class AuthController {

	@Autowired
	private UserSecurityService userSecurityService;

	@PostMapping("/signin")
	public ResponseEntity<ApiResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		return ResponseEntity.ok(userSecurityService.signin(loginRequest));
	}

	@PostMapping("/signup")
	public ResponseEntity<ApiResponse> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		return ResponseEntity.ok(userSecurityService.signUp(signUpRequest));
	}
}
