const {describe, it, after, before } = require('mocha');
const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const Page = require('../lib/funciones-firefox-local');
process.on('unhandledRejection', () => {});


( async function example() {

    try {

        describe("Pruebas de ingregación en servidor remoto Selenium navegador Firefox", async function() {
            this.timeout(60000);
            var driver, page;

            beforeEach(async() => {
                page = new Page();
                driver = page.driver;
                await page.visit('http://35.226.67.134:8000/');
                // await page.visit('http://localhost/');
            });

            afterEach(async() => {
                await page.quit();
            });

            it('Login éxitoso', async() => {
                const result = await page.login('admin', 'admin');
                console.log(result);
                expect(result).equals('Bienvenido admin!');
            });

            it('Login fallido', async() => {
                const result = await page.login('admin', '12345');
                console.log(result);
                expect(result).equals('Inténtelo de nuevo!');
            });

            it('Agregar producto fallido', async() => {
                const result = await page.agregarProducto('admin', 'admin', false);
                expect(result).equals('INGRESE TODOS LOS CAMPOS.');
            });

            it('Agregar producto éxitoso', async() => {
                const result = await page.agregarProducto('admin', 'admin', true);
                expect(result).equals('Producto Agregado correctamente');
            });


        });
        
    } catch (ex) {
        console.log(new Error(ex.message));
    } finally {

    }

})();
