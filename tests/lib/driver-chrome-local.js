const {builder, By, until, key, Builder} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');


let options = new chrome.Options();
options.addArguments('disable-infobars');
options.addArguments('window-size=1920,1080');
// options.addArguments('--headless');
options.setUserPreferences({
    credential_enable_service: false
});

var Page = function() {

    this.driver = new Builder()
        .setChromeOptions(options)
        .forBrowser('chrome')
        .build();

    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };

    this.quit = async function() {
        return await this.driver.quit();
    };

    this.findById = async function(id) {
        await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
        return (await this.driver).findElement(By.id(id));
    };

    this.findByName = async function(name) {
        await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
        return (await this.driver).findElement(By.name(name));
    };

    this.write = async function(el, txt) {
        return await el.sendKeys(txt);
    };

};

module.exports = Page;
