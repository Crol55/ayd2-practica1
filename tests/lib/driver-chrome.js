const {builder, By, until, key, Builder} = require('selenium-webdriver');

SeleniumServer = require('selenium-webdriver/remote').SeleniumServer ;

// const chrome = require('selenium-webdriver/chrome');
// const chromedriver = require('chromedriver');
// const { SeleniumServer } = require('selenium-webdriver/remote');

// var cbtHub = 'http://35.226.67.134:4000/wd/hub';
var cbtHub = `http://${process.env.HUB_HOST}:4444/wd/hub`

var caps = {
    name: 'Chrome Test',
    setPageLoadStrategy: 'eager',
    browserName: 'chrome',
    browserVersion: '85.0.4183.83'
};

// let options = new chrome.Options();
// options.addArguments('disable-infobars');
// options.addArguments('window-size=1920,1080');
// options.addArguments('--headless');
// options.setUserPreferences({
//     credential_enable_service: false
// });

var Page = function() {

    this.driver = new Builder()
        .withCapabilities(caps)
        .usingServer(cbtHub)
        .build();

    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };

    this.quit = async function() {
        return await this.driver.quit();
    };

    this.findById = async function(id) {
        await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
        return (await this.driver).findElement(By.id(id));
    };

    this.findByName = async function(name) {
        await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
        return (await this.driver).findElement(By.name(name));
    };

    this.write = async function(el, txt) {
        return await el.sendKeys(txt);
    };

};

module.exports = Page;
