const {until} = require('selenium-webdriver');
const fakedata = require('../utils/fakedata');
const Page = require('./driver-firefox');

const fakeNameKeyword = fakedata.nameKeyword;

let searchInput, searchButton, resultStat;
let usernameInput, passwordInput, signinButton, alertText;


Page.prototype.login = async function(username, password) {
    
    searchButton = await this.findById("btn-login");

    await searchButton.click();
    
    usernameInput = await this.findById("username");
    
    passwordInput = await this.findById("password");
    

    await usernameInput.click();
    await usernameInput.clear();

    await this.write(usernameInput, username);
    await this.write(passwordInput, password);

    signinButton = await this.findById("signin");

    await signinButton.click();

    // Wait for the alert to be displayed
    await this.driver.wait(until.alertIsPresent());

    // Store the alert in a variable
    let alert = await this.driver.switchTo().alert();

    //Store the alert text in a variable
    let alertText = await alert.getText();

    //Press the OK button
    await alert.accept();


    return alertText;
};


Page.prototype.agregarProducto = async function(username, password, success) {
    
    searchButton = await this.findById("btn-login");

    await searchButton.click();
    
    usernameInput = await this.findById("username");
    
    passwordInput = await this.findById("password");
    

    await usernameInput.click();
    await usernameInput.clear();

    await this.write(usernameInput, username);
    await this.write(passwordInput, password);

    signinButton = await this.findById("signin");

    await signinButton.click();

    // Wait for the alert to be displayed
    await this.driver.wait(until.alertIsPresent());

    // Store the alert in a variable
    let alert = await this.driver.switchTo().alert();

    //Store the alert text in a variable
    let alertText = await alert.getText();

    //Press the OK button
    await alert.accept();


    if (!success) {

        let addProductButton = await this.findById('agregarProducto');
    
        await addProductButton.click();
    
        // Wait for the alert to be displayed
        await this.driver.wait(until.alertIsPresent());
    
        // Store the alert in a variable
        alert = await this.driver.switchTo().alert();
    
        //Store the alert text in a variable
        alertText = await alert.getText();
    
        //Press the OK button
        await alert.accept();

    } else {

        let inputName, inputPath, inputDescription, inputPrice;
        inputName = await this.findById("productName");
        inputPath = await this.findById("imagePath");
        inputDescription = await this.findById("description");
        inputPrice = await this.findById("price");

        await this.write(inputName, fakeNameKeyword);
        await this.write(inputPath, 'https://static.kemikcdn.com/2019/11/4PF42LAABM-1.jpg');
        await this.write(inputDescription, fakeNameKeyword + ' description');
        await this.write(inputPrice, '50');

        let addProductButton = await this.findById('agregarProducto');

        await addProductButton.click();
    
        // Wait for the alert to be displayed
        await this.driver.wait(until.alertIsPresent());
    
        // Store the alert in a variable
        alert = await this.driver.switchTo().alert();
    
        //Store the alert text in a variable
        alertText = await alert.getText();
    
        //Press the OK button
        await alert.accept();


    }

    return alertText;
};


module.exports = Page;