const {builder, By, until, key, Builder} = require('selenium-webdriver');

SeleniumServer = require('selenium-webdriver/remote').SeleniumServer ;


// var cbtHub = 'http://35.226.67.134:4000/wd/hub';
var cbtHub = `http://${process.env.HUB_HOST}:4444/wd/hub`

var caps = {
    name: 'Firefox Test',
    setPageLoadStrategy: 'eager',
    browserName: 'firefox',
    browserVersion: '71.0'
};

var Page = function() {

    this.driver = new Builder()
        .withCapabilities(caps)
        .usingServer(cbtHub)
        .build();

    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    };

    this.quit = async function() {
        return await this.driver.quit();
    };

    this.findById = async function(id) {
        await this.driver.wait(until.elementLocated(By.id(id)), 15000, 'Looking for element');
        return (await this.driver).findElement(By.id(id));
    };

    this.findByName = async function(name) {
        await this.driver.wait(until.elementLocated(By.name(name)), 15000, 'Looking for element');
        return (await this.driver).findElement(By.name(name));
    };

    this.write = async function(el, txt) {
        return await el.sendKeys(txt);
    };

};

module.exports = Page;
