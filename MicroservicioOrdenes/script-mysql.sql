
CREATE DATABASE Delivery;

use `Delivery`;

create table prueba (
	id int auto_increment,
    nombre varchar(30) not null,
    primary key(id)
);


create table USERS (
	id bigint not null,
    nombre varchar(25) not null,
    primary key(id)
);
insert into USERS(id,nombre) values (1,'carlitros');


CREATE TABLE if not exists Catalog (
  idCatalog int(11) NOT NULL AUTO_INCREMENT,
  ProductName varchar(250) NOT NULL,
  ImagePath varchar(250) DEFAULT NULL,
  Description varchar(250) DEFAULT NULL,
  Price varchar(45) DEFAULT NULL,
  DateAdded datetime NOT NULL,
  PRIMARY KEY (`idCatalog`)
) ENGINE=InnoDB AUTO_INCREMENT=2;

insert into Catalog (ProductName, ImagePath, Description, Price, DateAdded) values ('Fried Chicken', 'https://i.pinimg.com/564x/90/e4/e9/90e4e9cae6ded1516d2fae45a25fe9ee.jpg', 'Delicious Fried Chicken', '15.75', now());

-- ----------TABLA ORDENES
CREATE TABLE IF NOT EXISTS ORDEN (

    orden_id     int auto_increment, 
    estado       varchar (45) not null,
    tipo_entrega varchar (15) not null,
    direccion    varchar (80), 
    precio_total decimal (7,2) not null,
    fecha datetime not null,
    USERS_id   bigint,
    PRIMARY KEY (orden_id),
    constraint fk_users foreign key (USERS_id) references USERS(id)    
);

insert into ORDEN(estado, tipo_entrega, direccion, precio_total,fecha,USERS_id) values ('Nueva Orden', 'Domicilio','5ta calle blah blah', 51.25,now(),1);
insert into ORDEN(estado, tipo_entrega, direccion, precio_total,fecha,USERS_id) values ('Nueva Orden', 'Restaurante',null, 35.55,now(), 1); 
insert into ORDEN(estado, tipo_entrega, direccion, precio_total,fecha,USERS_id) values ('Nueva Orden', 'Restaurante',null, 35.55,now(), null);  -- ORDEN SIN usuario

    
-- TABLA HISTORIAL_ORDEN
CREATE TABLE IF NOT EXISTS HISTORIAL_ORDEN (
    cantidad int not null,
    ORDEN_orden_id int not null,
    CATALOG_idcatalog int not null, 
    constraint fk_orden foreign key (ORDEN_orden_id ) references ORDEN(orden_id),
    constraint fk_catalog foreign key (CATALOG_idcatalog) references Catalog(idCatalog)
);
insert into HISTORIAL_ORDEN (cantidad, ORDEN_orden_id, CATALOG_idcatalog) values (2, 1, 1);
insert into HISTORIAL_ORDEN (cantidad, ORDEN_orden_id, CATALOG_idcatalog) values (1, 1, 1);


ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
FLUSH PRIVILEGES;

ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY '1234';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;




