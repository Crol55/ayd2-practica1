
var express = require('express');
var router = express.Router(); // para manejar todas las rutas que entren al servidor


const controlador = require('../controlador/controlador'); // El codigo que se utilizara cuando llegue una ruta al servidor

// PETICIONES HTTP A LA APP
router.post('/ordenes', controlador.create);
router.get ('/ordenes/usuario', controlador.read); 
router.get ('/ordenes/getall', controlador.readAll);
router.put ('/ordenes', controlador.update);
router.delete ('/ordenes/deleteOrden', controlador.delete);

module.exports = router;  // este archivo se utiliza en servidor.js