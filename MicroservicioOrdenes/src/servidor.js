
const express = require ('express');
const puerto = process.env.PORT || 4000;
const cors = require('cors');

var rutas = require('./rutas/rutas'); // Archivo que respondera a las rutas solicitadas

const app = express();
    app.use(cors());
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json()); // leer (Json Request)
    app.use('/', rutas);

app.listen( puerto, () =>{
    console.log("Servidor funcionando en el puerto "+ puerto);
});