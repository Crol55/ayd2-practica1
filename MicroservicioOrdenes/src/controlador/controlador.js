
const mysqlConnexion = require('../conexionBD');


function prueba(){
    console.log("me ejecuto?");
    mysqlConnexion.query( 'SELECT * FROM users WHERE id = 1' );
    mysqlConnexion.query('SELECT * FROM prueba', (error, rows,fields) =>{
        if(!error){
            console.log(rows);
        }else {
            console.log(error);
        }
    })
}

async function insert_orden (id, tipo_entrega, direccion, precio, productos)
{ 
    /*//let msg_estado = ""; //Sera lo que se respondera al frontend
    if (id ==='0')  id = null;
    if (direccion ==='')    direccion = null;
    const query = `insert into ORDEN(estado, tipo_entrega, direccion, precio_total,fecha,USERS_id) values
    ('Nueva orden', ${mysqlConnexion.escape(tipo_entrega)}, ${mysqlConnexion.escape(direccion)}, ${mysqlConnexion.escape(precio)}, now(), ${mysqlConnexion.escape(id)})`;

    const query = 'insert into ORDEN(estado, tipo_entrega, direccion, precio_total,fecha,USERS_id) values (?, ?, ?, ?, now(), ?)';
    mysqlConnexion.query(query,['Nueva orden',tipo_entrega, direccion, precio, id], (error, rows, fields) =>{
        if(!error){
            //console.log(rows); 
            let orden_id = rows.insertId
            let query = `insert into HISTORIAL_ORDEN (cantidad, ORDEN_orden_id, CATALOG_idcatalog) values (?,?,?)`;

            productos.forEach(producto => {
                mysqlConnexion.query(query,[producto.cantidad, orden_id, producto.id], (err,rows,fields) =>{
                    if(!err){
                        console.log(rows);
                        msg_estado = "Exito! Su orden ha sido registrada correctamente";
                    }else {
                        msg_estado = "Uups! Hubo un error agregar productos a su orden";
                    }
                })
            });

        }else {
            msg_estado = "Uups! Hubo un error al crear un orden";
            console.log(error);
        }
    });
    return msg_estado;*/
}


function productos_toJson(rows){ // convierte la entrada de productos para que tenga un formato tipo json
    
    let mJson = '{\n"orden":[\n';
    let pivote = '';
    for (let i =0; i < rows.length; i++)
    {
    // {
        if(pivote != rows[i].orden_id) // Cuando abrir parentesis
        {
            const {orden_id,estado,tipo_entrega,direccion,precio_total,fecha} = rows[i];
            mJson += '{\n';
            pivote = rows[i].orden_id;  
            mJson += '"orden_id":"'+orden_id+'",\n';
            mJson += '"tipo_entrega":"'+tipo_entrega+'",\n';
            mJson += '"direccion":"'+direccion+'",\n';
            mJson += '"precio_total":"'+precio_total+'",\n';
            mJson += '"estado":"'+estado+'",\n';
            mJson += '"fecha":"'+fecha+'"\n';
            mJson += ',"productos":[\n';
            
        }

        mJson += '\t{';
        const {cantidad, ProductName, ImagePath,Description} = rows[i];
        mJson += '"cantidad":"'+cantidad+'",';
        mJson += '"ProductName":"'+ProductName+'",';
        mJson += '"ImagePath":"'+ImagePath+'",';
        mJson += '"Descripcion":"'+Description+'"';
        //console.log(pivote+'-');
        mJson += '}';
        if(i < rows.length -1){
            if(pivote != rows[i+1].orden_id) // Cuando cerrar parentesis
            mJson += '\n\t]\n},';
            else 
                mJson += ',\n'; // es otro producto

        }else {
            mJson +=']\n}';
        }
                //}
    }
                mJson += ']}';
return mJson;
}

const visualizar = (req, res) =>{ // GET (id especifico)

    //console.log(req.query)
    const {users_id} = req.query; 
    //console.log(orden_id);
    mysqlConnexion.query(`select orden_id,estado, tipo_entrega,direccion,precio_total,fecha,cantidad,ProductName,ImagePath,Description from ORDEN inner join HISTORIAL_ORDEN 
    ON orden_id = ORDEN_orden_id inner join Catalog on CATALOG_idcatalog = idCatalog where USERS_id =? order by orden_id`,[users_id], (error, rows,fields) =>{
        if(!error){
            //console.log(rows);
            let mJson = productos_toJson(rows);
            res.send(mJson);

        }else {
            console.log(error);
        }
    })
};


const visualizar_todo = (req, res) => { // GET ALL

    const query = `select orden_id,estado, tipo_entrega,direccion,precio_total,fecha,cantidad,ProductName,ImagePath,Description from ORDEN inner join HISTORIAL_ORDEN 
    ON orden_id = ORDEN_orden_id inner join Catalog on CATALOG_idcatalog = idCatalog order by orden_id` ;

    mysqlConnexion.query(query, (error, rows,fields) =>{
        if(!error){
            
            if(rows.length > 0)
            {
                var mJson = productos_toJson(rows); // convertir las tuplas de la consulta a formato Json
            }
            res.send( mJson);

        }else {
            console.log(error);
        }
    }) 
}


const crear =  (req, res) => { // POST
    //console.log(req.body);
    var {id,tipo,direccion,precio_total,productos} = req.body;
    
    if (id ==='0')  id = null;
    if (direccion ==='')    direccion = null;
    
    const query = 'insert into ORDEN(estado, tipo_entrega, direccion, precio_total,fecha,USERS_id) values (?, ?, ?, ?, now(), ?)';
    mysqlConnexion.query(query,['Nueva orden',tipo, direccion, precio_total, id], (error, rows, fields) =>{

        if(!error){

            let orden_id = rows.insertId
            let query = `insert into HISTORIAL_ORDEN (cantidad, ORDEN_orden_id, CATALOG_idcatalog) values (?,?,?)`;

            productos.forEach(producto => {
                mysqlConnexion.query(query,[producto.cantidad, orden_id, producto.id], (err,rows,fields) =>{
                    if(!err){
                        //console.log(rows);
                        //res.send("Exito! Su orden ha sido registrada correctamente");
                    }else {
                        console.log(err);
                       // res.send("Uups! Hubo un error agregar productos a su orden");
                    }
                })
            });
            res.send(JSON.stringify( {respuesta: 'Exito! Orden creada correctamente'} ));
        }else {
           res.send(JSON.stringify( {respuesta: 'Uups! Hubo un error al crear un orden'} ));
            console.log(error);
            
        }
    });
    
}

const actualizar = (req, res) =>{ // PUT { orden -> solo restaurante}
    //res.send("peticion put, para actualizar una orden");

    let query = `update ORDEN set estado = ? where orden_id = ?`;
    let {orden_id, estado} = req.body;
    mysqlConnexion.query(query,[estado, orden_id], (error, rows,fields) =>{
        if(!error){
            if(rows.affectedRows)
                res.send( JSON.stringify( { respuesta : 'Exito! Orden actualizada correctamente'}));
            else 
            res.send( JSON.stringify( { respuesta : 'ups! Orden NO ACTUALIZADA'}));
        }else {
            console.log(error);
        }
    }) 
    
}

const eliminar = (req,res) =>{  
    //res.send("peticion delete, para eliminar una orden");
    const {id} = req.query;
    let query = 'delete from HISTORIAL_ORDEN where ORDEN_orden_id = ?';
    mysqlConnexion.query(query,[id], (error, rows,fields) =>{
        if(!error){
            if(rows.affectedRows)
            {
                query = 'delete from ORDEN where orden_id = ?';
                mysqlConnexion.query(query,[id], (error, rows,fields) =>{
                    if(!error){
                        if(rows.affectedRows)
                        {
                            res.send( JSON.stringify( { respuesta : 'Correcto! Se ha eliminado la orden y sus productos asociados'})); 
                        }
                        else 
                        res.send( JSON.stringify( { respuesta : 'ups! No existen ordenes con ese ID, error al eliminar en ORDENES'}));
                    }else {
                        console.log(error);
                        res.send( JSON.stringify( { respuesta : 'ups! Error interno en BD'}));
                    }
                })
            }
            else 
            res.send( JSON.stringify( { respuesta : 'ups! No existen historial de ordenes con ese ID'}));
        }else {
            console.log(error);
            res.send( JSON.stringify( { respuesta : 'ups! Error interno en BD'}));
        }
    }) 
}

module.exports = { // MUY IMPORTANTE PARA VER LAS REFERENCIAS
    //prueba:prueba,
    read:visualizar,
    readAll:visualizar_todo,
    create:crear,
    update:actualizar,
    delete:eliminar
}

