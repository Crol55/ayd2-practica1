
const mysql = require ('mysql');

//const host = process.env.HOST || '192.168.1.22'; 
const host = process.env.HOST || 'mysql-database-practica1';
const db = process.env.DB || 'Delivery';
const user = process.env.USER || 'root';
const password = process.env.PASS || '1234';



/*
var connection = mysql.createConnection({
    host : host,
    database : db,
    user : user,
    port: 33060,
    password : password
});

connection.connect(function(err) {
    if (err) {
        console.error('Error de conexion: ' + err.stack);
        return;
    }
    console.log('Conectado con el identificador ' + connection.threadId);
});*/


var pool = mysql.createPool({
    connectionLimit: 10,
    host: host,
   // port:33060,
    user: user,
    password: password,
    database: db,
    multipleStatements: true
})



module.exports = pool;


