
echo "Subiendo imagenes a Container Registry"
filename="version"

while read -r line; do
    echo "$line"
    docker tag ayd2-practica1_ordenesmicroservicio $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_ordenesmicroservicio${line}
    docker tag ayd2-practica1_catalogmicroservice $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_catalogmicroservice${line}  
    docker tag ayd2-practica1_aydelivery $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_aydelivery${line}
    docker tag ayd2-practica1_user-auth $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_user-auth${line}
    docker tag ayd2-practica1_mysql-db $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_mysql-db${line}
    docker push $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_ordenesmicroservicio${line}
    docker push $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_catalogmicroservice${line}
    docker push $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_aydelivery${line}
    docker push $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_user-auth${line}
    docker push $CI_REGISTRY/crol55/ayd2-practica1:ayd2-practica1_mysql-db${line}
echo "FIN DEL SCRIPT"
done < "$filename"
